import { Component } from '@angular/core';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
    player: MediaObject;
    isPlatformReady: boolean;
    messages: string[] = [];

    public constructor(
        private backgroundMode: BackgroundMode,
        private media: Media,
        private platform: Platform,
    ) {
        this.platform.ready()
            .then(() => { this.isPlatformReady = true; })
            .catch(() => this.messages.push('Could not initialize platform!'));
    }

    public play(): void {
        this.player = this.media.create('http://stream01.superfly.fm:8080/live128');
        this.player.play();
        this.backgroundMode.enable();
        this.messages.push('Started to play');
    }

    public stop(): void {
        this.player.stop();
        this.player.release();
        this.backgroundMode.disable();
        this.player = null;
        this.messages.push('Stopped player');
    }
}
