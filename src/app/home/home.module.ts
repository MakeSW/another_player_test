import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Media } from '@ionic-native/media/ngx';
import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  providers: [
    Media,
    BackgroundMode,
  ],
  declarations: [ HomePage ]
})
export class HomePageModule {}
