#!/bin/bash

# Issues with Android -> see https://stackoverflow.com/questions/52513103/ionic-4-platform-add-android-error-code-enolocal
# We have to use ionic cordova platform add android
# Then we can build it -> not sure why that's the case ...


echo "This is a simple setup script - it requires"
echo "  node and npm"
echo "  ionic"
echo "  xcode tool chain"

echo "Removing IOS directory - if it exists"
rm -rf ./ios

echo "Installing all the requirements"

npm install

echo "Building web app"

ionic build

echo "Setting up basic ionic project"

ionic capacitor sync

echo "Adding iOS plattform"

ionic capacitor add ios

echo "Setting up project plist file"

cp ./workarounds/ios/App/Info.plist ./ios/App/App/Info.plist

echo "Starting project in xCode"

ionic capacitor open ios